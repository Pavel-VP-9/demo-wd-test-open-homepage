import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePageTest {
    @Test
    public void testOnLiner() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.onliner.by/");
        String locatorLogo = "//*[@id=\"container\"]/div/div/header/div[3]/div/div[1]/a/img";
        By byLocatorLogo = By.xpath(locatorLogo);
        WebElement elementLogo = driver.findElement(byLocatorLogo);
        Assert.assertTrue(elementLogo.isDisplayed());
        System.out.println("OnLiner logo is displayed – Assert passed");
        driver.quit();

    }

    @Test
    public void testAmazon() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com/");
        String locatorLogo = "//*[@id=\"nav-logo-sprites\"] ";
        // String locatorLogo = "//*[@id=\"nav-logo\"]";
        By byLocatorLogo = By.xpath(locatorLogo);
        WebElement elementLogo = driver.findElement(byLocatorLogo);
        Assert.assertTrue(elementLogo.isDisplayed());
        System.out.println("Amazon logo is displayed – Assert passed");
        driver.quit();
    }

    @Test
    public void testTicketPro() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.ticketpro.by/");
        String locatorLogo = "/html/body/div[2]/header/div/div[1]/div[1]/div/a/img";
        By byLocatorLogo = By.xpath(locatorLogo);
        WebElement elementLogo = driver.findElement(byLocatorLogo);
        Assert.assertTrue(elementLogo.isDisplayed());
        System.out.println("TicketPro logo is displayed – Assert passed");
        driver.quit();
    }
    @Test
    public void testAlatanTour() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://alatantour.by/");
        String locatorLogo = "/html/body/header/div/div/div/div/a/img";
        By byLocatorLogo = By.xpath(locatorLogo);
        WebElement elementLogo = driver.findElement(byLocatorLogo);
        Assert.assertTrue(elementLogo.isDisplayed());
        System.out.println("AtalanTour logo is displayed – Assert passed");
        driver.quit();
    }
    @Test
    public void testOlx() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(" https://www.olx.pl/ ");
        String locatorLogo = "//*[@id=\"headerLogo\"]";
        By byLocatorLogo = By.xpath(locatorLogo);
        WebElement elementLogo = driver.findElement(byLocatorLogo);
        Assert.assertTrue(elementLogo.isDisplayed());
        System.out.println("Olx logo is displayed – Assert passed");
        driver.quit();
    }
    @Test
    public void testTripadvisor() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.tripadvisor.com/ ");
        String locatorLogo = "//*[@id=\"lithium-root\"]/header/div/nav/h1/picture/img";
        By byLocatorLogo = By.xpath(locatorLogo);
        WebElement elementLogo = driver.findElement(byLocatorLogo);
        Assert.assertTrue(elementLogo.isDisplayed());
        System.out.println("Tripadvisor logo is displayed – Assert passed");
        driver.quit();
    }
}
